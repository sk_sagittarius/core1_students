﻿using EmptyProject.Interfaces.Impl;
using EmptyProject.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmptyProject.Controllers
{
    public class HomeController : Controller
    {
        private IStudentRepository _studentRepository;
        public HomeController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public IActionResult Index()
        {
            //ViewBag.Variable = "some text";
            //ViewData["Some data"] = "another text";

            var students = _studentRepository.GetAll();
            return View(students);
        }

        public IActionResult Details(int id)
        {
            var student = _studentRepository.Get(id);
            return View(student);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Student student)
        {
            var isSuccessful = _studentRepository.Add(student);
            if (isSuccessful) return RedirectToAction("Index");

            return View();
        }

        [HttpGet]
        public IActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Edit(int id, string name)
        {
            _studentRepository.Edit(id, name);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            _studentRepository.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
