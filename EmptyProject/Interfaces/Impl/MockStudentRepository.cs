﻿using EmptyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmptyProject.Interfaces.Impl
{
    public class MockStudentRepository : IStudentRepository
    {
        IList<Student> _students;
        public MockStudentRepository()
        {
            _students = new List<Student>()
            {
                new Student {Id = 1, Name = "First" },
                new Student {Id = 2, Name = "Second" },
                new Student {Id = 3, Name = "Third" },
            };
        }

        public Student Get(int id)
        {
            //return students.Where(x => x.Id == id).ToString();
            return _students.FirstOrDefault(x => x.Id == id);
        }

        //public string Get(string name)
        //{
        //    //return students.Where(x => x.Name == name).ToString();
        //}

        public IEnumerable<Student> GetAll()
        {
            // list?
            return _students;
        }

        public void Create(Student student)
        {
            //students.Add(student);
            
        }

        public bool Add(Student student)
        {
            try
            {
                var maxid = _students.Max(x => x.Id);
                student.Id = maxid + 1;
                _students.Add(student);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void Edit(int id, string name)
        {
            _students.FirstOrDefault(x => x.Id == id).Name = name;
        }

        public void Delete(int id)
        {
            var student = _students.FirstOrDefault(x => x.Id == id);
            _students.Remove(student);
        }
    }
}
