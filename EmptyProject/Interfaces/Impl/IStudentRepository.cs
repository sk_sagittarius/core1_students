﻿using EmptyProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmptyProject.Interfaces.Impl
{
    public interface IStudentRepository
    {
        Student Get(int id);
        //string Get(string name);
        IEnumerable<Student> GetAll();
        bool Add(Student student);

        //void Create(Student student);
        void Edit(int id, string name);
        void Delete(int id);
    }
}
